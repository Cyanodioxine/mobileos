var router = require('express').Router();
var AppManager = require('../applications/AppManager');

router.get('/', function(req, res, next) {
	var apps = AppManager.getInstalledApps();

	res.render('pages/home', {
		useLayout: true,
		apps: apps
	});
});

module.exports = router;