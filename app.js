var config = global.config = require('./config.json');

global['root-dir'] = __dirname;

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var moment = require('moment');
var ejs = require('ejs'); //include(body)

var app = express();
app.disable('x-powered-by');

app.locals.LZW = {
	encode: function(r){for(var n,t={},e=(r+"").split(""),o=[],h=e[0],a=256,l=1;l<e.length;l++)n=e[l],null!=t[h+n]?h+=n:(o.push(h.length>1?t[h]:h.charCodeAt(0)),t[h+n]=a,a++,h=n);o.push(h.length>1?t[h]:h.charCodeAt(0));for(var l=0;l<o.length;l++)o[l]=String.fromCharCode(o[l]);return o.join("")},
	decode: function(r){for(var n,t={},e=(r+"").split(""),o=e[0],h=o,a=[o],l=256,c=1;c<e.length;c++){var u=e[c].charCodeAt(0);n=256>u?e[c]:t[u]?t[u]:h+o,a.push(n),o=n.charAt(0),t[l]=h+o,l++,h=n}return a.join("")}
};

app.locals.pluralize = function(number, singular, plural) {
	return (number != 1) ? plural : singular;
}

app.locals.timegap = function(start) {
	var now = new Date();
	var then = moment(new Date(start));

	return then.from(now);
}

app.locals.shuffle = function(array) {
	var out = [];
	var currentIndex = array.length, temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		out[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return out;
}

app.set('views', [path.join(__dirname, 'views'), path.join(__dirname, 'applications')]);
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/apps', express.static(__dirname + '/applications/apps'));

app.use(function (req, res, next) {
	var send = res.send;
	var render = res.render;

	res.send = function(data) {
		data.status = data.status || 200;
		send.call(this, data);
	}

	res.renderString = function(page, data, callback) {
		if(!data)
			data = {
				useLayout: true,
				title: "",
				status: 200
			};

		data.title = data.title || "";
		data.status = data.status || 200;

		if(data['useLayout'] === true) {
			data.body = ejs.render(page, data);
			ejs.renderFile(__dirname + '/views/view/layout.ejs', data, function(err, layout) {
				if(err) return next(err);
				send.call(res, layout);
			});
		} else {
			send.call(res, page);
		}
	}

	res.render = function(page, data, callback) {
		if(!data)
			data = {};

		if(!data.title)
			data.title = "";

		data.status = data.status || 200;

		if(data['useLayout'] === true) {
			ejs.renderFile(__dirname + '/views/view/' + page + '.ejs', data, function(err, rendered) {
				if(err) return next(err);
				data.body = rendered;

				ejs.renderFile(__dirname + '/views/view/layout.ejs', data, function(err, layout) {
					if(err) return next(err);
					send.call(res, layout);
				});
			});
		} else {
			render.call(res, page, data, callback);
		}
	}
	next();
});

app.use('/system/ui/home', require('./routes/system/home'));
app.use('/', require('./routes/home'));
app.use('/app', require('./applications/AppManager'));

app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

module.exports = app;