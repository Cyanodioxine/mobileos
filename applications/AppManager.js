var installedApps = require('./installed.json')['apps'];
var fs = require('fs');
var ejs = require('ejs');

var root = global['root-dir'];

var apps = {};

for(var i in installedApps) {
	var app = installedApps[i];
	var appcfg = require('./apps/' + app + '/app.json');
	console.log('Loading app "' + app + '" (v' + appcfg['version'] + ')');
	var main = require('./apps/' + app + '/' + appcfg['main']);
	main.onLoad();
	apps[app] = {
		config: appcfg,
		main: main
	};
}

var router = require('express').Router();

router.get('/:app', function(req, res, next) {
	var params = req.params;

	if(params.app) {
		var appid = params.app;
		if(installedApps.includes(appid)) {
			var out = apps[appid].main.onLaunch(req.params);
			res.renderString(out);
		} else {
			next(new Error('No app found with id "' + appid + '"'));
		}
	} else {
		next(new Error('Lol no.'));
	}
});

router.get('/:app/icon', function(req, res, next) {
	var params = req.params;

	if(params.app) {
		var appid = params.app;
		if(installedApps.includes(appid)) {
			res.sendFile(global['root-dir'] + '/applications/apps/' + appid + '/icon.png');
		} else {
			next(new Error('No app found with id "' + appid + '"'));
		}
	} else {
		next(new Error('Lol no.'));
	}
});


router.get('/:app/splash', function(req, res, next) {
	var params = req.params;

	if(params.app) {
		var appid = params.app;
		if(installedApps.includes(appid)) {
			res.sendFile(global['root-dir'] + '/applications/apps/' + appid + '/splash.png');
		} else {
			next(new Error('No app found with id "' + appid + '"'));
		}
	} else {
		next(new Error('Lol no.'));
	}
});

router.get('/:app/config', function(req, res, next) {
	var params = req.params;

	if(params.app) {
		var appid = params.app;
		if(installedApps.includes(appid)) {
			res.send(apps[appid].config);
		} else {
			next(new Error('No app found with id "' + appid + '"'));
		}
	} else {
		next(new Error('Lol no.'));
	}
});

module.exports = router;
module.exports.getApplication = function(appid) {
	if(installedApps.includes(appid)) {
		return apps[appid].main;
	} else {
		return null;
	}
}
module.exports.getInstalledApps = function() {
	return apps;
}