UI.Navigation.Bottom = {
	Color: '#000',
	TextColor: '#000',
	Size: '50px',
	Visible: true
}

UI.Navigation.Notifications = {
	Color: '#000',
	TextColor: '#000',
	TimeFormat: '12h', // Opts: 12h, 24h
	Size: '20px',
	Visible: true
}

UI.Navigation.Notifications.Hide = function() {

}

UI.Navigation.Notifications.Show = function() {

}

$(function() {
	var clock = $('#mui-notifbar .time');
	var now = new Date();
	clock.text((now.getHours() % 12 || 12) + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes()));

	setInterval(function() {
		now = new Date();
		clock.text((now.getHours() % 12 || 12) + ':' + (now.getMinutes() < 10 ? '0' + now.getMinutes() : now.getMinutes()));
	}, 1000);


	HandleLongPress('#mui-softbuttons-back', 750, function() {
		Events.propagate('ui-quickaction-request', []);
	});

	HandleLongPress('#mui-softbuttons-home', 750, function() {
		Events.propagate('ui-assist-request', []);
	});

	HandleLongPress('#mui-softbuttons-recent', 750, function() {
		Events.propagate('ui-context-request', []);
	});
});