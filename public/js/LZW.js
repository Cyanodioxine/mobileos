var LZW = {
	encode: function(r){for(var n,t={},e=(r+"").split(""),o=[],h=e[0],a=256,l=1;l<e.length;l++)n=e[l],null!=t[h+n]?h+=n:(o.push(h.length>1?t[h]:h.charCodeAt(0)),t[h+n]=a,a++,h=n);o.push(h.length>1?t[h]:h.charCodeAt(0));for(var l=0;l<o.length;l++)o[l]=String.fromCharCode(o[l]);return o.join("")},
	decode: function(r){for(var n,t={},e=(r+"").split(""),o=e[0],h=o,a=[o],l=256,c=1;c<e.length;c++){var u=e[c].charCodeAt(0);n=256>u?e[c]:t[u]?t[u]:h+o,a.push(n),o=n.charAt(0),t[l]=h+o,l++,h=n}return a.join("")}
};