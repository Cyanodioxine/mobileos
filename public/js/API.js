System = { };

UI = {
	Navigation: {},
	Transition: {}
}

var Events = {
	handlers: {},
	on: function(event, handler) {
		if(this.handlers.hasOwnProperty(event)) {
			this.handlers[event].push(handler);
		} else {
			this.handlers[event] = [
				handler
			];
		}
	},
	propagate: function(event, data) {
		if(this.handlers.hasOwnProperty(event)) {
			var handlers = this.handlers[event];

			for(var i = 0; i < handlers.length; i++) {
				handlers[i].apply(null, data);
			}
		}
	}
}

System.OpenApplication = function(appid) {
	if(!appid) return;
	Events.propagate('app-open', [appid]);
	$('#AppContainer').empty();
	var logo = $('<img splash-image src="/app/' + appid + '/splash">');
	logo.appendTo($('#AppContainer'));
	logo.on('load', function() {
		var frame = $('<iframe src="/app/' + appid + '" style="display: none;"></iframe>');
		frame.on('load', function() {
			logo.remove();
			var page = $(this).contents().find('#body').html();
			$('#AppContainer').html(page); //TODO: Replace with better system
		});
		frame.appendTo($('#AppContainer'))
	});
	$('#AppContainer').attr('active', 'true');
}

System.CloseApplication = function() {
	$('#AppContainer').removeAttr('active');
	$('#AppContainer').empty();
}

function HandleLongPress(element, delay, handler) {
	var timer;
	$(element).on("mousedown",function(){
		timer = setTimeout(function(){
			handler();
		},delay);
	}).on("mouseup mouseleave",function(){
		clearTimeout(timer);
	});
}

$(function() {
	$(document).on('click', '[app-executor]', function(e) {
		e.preventDefault();
		System.OpenApplication($(this).attr('appid'));
	});
});