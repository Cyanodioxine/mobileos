//Super easy mobile detection
//Injects a hidden element (and a style for it), and uses the media query to determine whether you're on a mobile screen size
//Easy, right?
//Prerequisites: JQuery (duh)

mobile = false;

$(function() {
	var elem = $('<div id="mb-browser-mobiledetect"></div>');
	var style = $('<style>#mb-browser-mobiledetect { display: none; } @media (max-width: 768px) { #mb-browser-mobiledetect:before {content: "y";} }</style>');
	$('body').append(style);
	$('body').append(elem);

	$(window).resize(function() {
		recalc();
	});

	function recalc() { //TODO: Can haz optimize?
		var elem = $("#mb-browser-mobiledetect")[0];
		var data = window.getComputedStyle(elem,':before').content;
		mobile = (data.includes("y")) ? true : false;
	}

	recalc();
});