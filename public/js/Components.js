(function($) {
	$.fn.transmute = function(newType) {
		var e = this[0];
		var new_element = document.createElement(newType),
			old_attributes = e.attributes,
			new_attributes = new_element.attributes,
			child = e.firstChild;
		for(var i = 0, len = old_attributes.length; i < len; i++) {
			new_attributes.setNamedItem(old_attributes.item(i).cloneNode());
		}
		while(e.firstChild) {
			new_element.appendChild(e.firstChild);
		}
		e.parentNode.replaceChild(new_element, e);
		return $(new_element); // for chain... $(this)?  not working with multiple
	}
})(jQuery);
$(document).ready(update);

function update() {
	$('[columned]').each(function() {
		var t = $(this);
		var w = t.width();
		var columns = t.find('[column]');
		var size = (w / columns.length);
		t[0].style['text-align'] = 'center';

		columns.each(function(i) {
			$(this)[0].style.width = (size) + 'px';
		});
	});
}

$(function() {
	$('text').each(function() {
		var that = $(this);
		var size = that.attr('size') || '12px';
		var color = that.attr('color') || '#000';
		var centered = that.attr('centered');
		var align = that.attr('align');

		that = that.transmute('p');
		that.removeAttr('size');
		that.removeAttr('color');
		that.removeAttr('centered');
		if((typeof centered != 'undefined' && centered != null) || (typeof align != 'undefined' && align != null)) {
			that[0].style['text-align'] = (typeof centered != 'undefined' && centered != 'false') ? 'center' : align;
		}
		that[0].style.color = color;
		that[0].style['font-size'] = size;
	});

	$('element[type="button"]').each(function() {
		var that = $(this);
		that = that.transmute('button');
		that.removeAttr('type');
		that.toggleClass('ripple');
	});

	$('view[href]').each(function() {
		var that = $(this);
		that.attr('src', that.attr('href'));
		that.removeAttr('href');
		that.prop('style', 'width: 100%; height: 100%; border: none; padding: 0; margin: 0;');
		that.transmute('iframe');
	});

	$(window).on('resize', update);
	$(".nano").nanoScroller();
});