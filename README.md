### MobileOS ###
An Android clone written in Node + EJS + HTML5 for Chromium Kiosk Mode

### Why would you do this? ###
Because all of the ports of Android to Raspberry Pi are terrible. I wanted something that worked.